import React, { Component } from "react";
import dataShoes from "../data/dataShoes.json";
import ModalShoes from "./ModalShoes";
import ShoesList from "./ShoesList";
export default class BaiTapShoes extends Component {
    state = {
        gioHang: [],
    }
    themGioHang = (sanPham) => {
        // coppy ra mảng mới khi set State mới render lại component => render lại dom
       let capNhatGioHang = [...this.state.gioHang]
       let index = this.state.gioHang.findIndex(item => {
           return item.id ===  sanPham.id
        })
        if(index !== -1) {
            capNhatGioHang[index].quantity += 1
        }else {
            let newShoes = {
                ...sanPham,
                quantity: 1
            }
            capNhatGioHang.push(newShoes)
        }
        this.setState({
            gioHang: capNhatGioHang,
        })
    }

    xoaSanPham = (id) => {
        let capNhatGiohang = [...this.state.gioHang]
        // so sánh id để lấy ra được vị trí index
        let index = capNhatGiohang.findIndex(item => {
            return item.id === id
        })
        capNhatGiohang.splice(index, 1)
        this.setState({
            gioHang: capNhatGiohang
        })
    }
    
    tangGiamSoLuong = (id, tangGiam) => {
        let capNhatGioHang =[...this.state.gioHang]
        let index = capNhatGioHang.findIndex(item => {
            return item.id === id
        })
        if(index !== -1) {
            if(tangGiam==='add') {
                capNhatGioHang[index].quantity += 1
            }else {
                capNhatGioHang[index].quantity -= 1
            }
        }
        if(capNhatGioHang[index].quantity <= 0) {
            capNhatGioHang.splice(index, 1)
        }
        this.setState({
            gioHang: capNhatGioHang
        })
    }

  render() {
    let tongSoLuongGH = this.state.gioHang.reduce((tongSoLuong, item, index) => {
        return tongSoLuong += item.quantity
    }, 0)
    return (
      <div className="container">
      <h2 className="text-center text-danger">Bài tập shoes</h2>
        <ModalShoes modalGioHang={this.state.gioHang} xoaSanPham={this.xoaSanPham} tangGiamSoLuong={this.tangGiamSoLuong}/>
        <button className="mb-3" data-toggle="modal" data-target="#modelId"
        style={{display: 'block' ,marginLeft: 'auto'}}
        >
        <i className="las la-shopping-cart"></i>
        <span>({tongSoLuongGH})</span>
        </button>
        <ShoesList dataShoes={dataShoes} themGioHang={this.themGioHang}/>
      </div>
    );
  }
}
