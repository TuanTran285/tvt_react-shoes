import React, { Component } from "react";

export default class ModalShoes extends Component {
    renderGioHang = () => {
        return (
            this.props.modalGioHang.map((sanPham, index) => {
                return (
                    <tr key ={index}>
                    <td>{sanPham.id}</td>
                    <td><img style={{width: 80, height: 80}} src={sanPham.image} alt={sanPham.image} /></td>
                    <td>{sanPham.name}</td>
                    <td>{sanPham.price}$</td>
                    <td>
                      <button onClick={() => this.props.tangGiamSoLuong(sanPham.id, 'sub')} className="btn btn-danger">-</button>
                      <span className="mx-2">{sanPham.quantity}</span>
                      <button onClick={() => this.props.tangGiamSoLuong(sanPham.id, 'add')} className="btn btn-danger">+</button>
                    </td>
                    <td>{sanPham.price * sanPham.quantity}$</td>
                    <td><button className="btn btn-danger" onClick={() => {this.props.xoaSanPham(sanPham.id)}}>Xóa</button></td>
                </tr>
                )
            })
        )
    }
  render() {
    return (
      <div
        className="modal fade"
        id="modelId"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content" style={{width: '800px'}}>
            <div className="modal-header">
              <h5 className="modal-title">Modal title</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
                <table className="table">
                    <thead>
                        <tr>
                            <th>Mã</th>
                            <th>Hình ảnh</th>
                            <th>Tên</th>
                            <th>Giá</th>
                            <th>Số lượng</th>
                            <th>Tổng tiền</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                       {this.renderGioHang()}
                    </tbody>
                </table>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button type="button" className="btn btn-primary">
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
