import React, { Component } from "react";

export default class Shoes extends Component {
  render() {
    let { shoes } = this.props;
    return (
      <div className="card col-3" style={{height: 500, position:'relative'}}>
        <img className="card-img-top" src={shoes.image} style={{height: 300}} alt='' />
        <div className="card-body">
          <h4 className="card-title">{shoes.name}</h4>
          <p className="card-text">Giá: {shoes.price}$</p>
          <button className="btn btn-danger"
           style={{position: 'absolute', left: '30px', bottom: '20px'}}
           onClick={() => this.props.themGioHang(shoes)}
           >Thêm giỏ hàng</button>
        </div>
      </div>
    );
  }
}
