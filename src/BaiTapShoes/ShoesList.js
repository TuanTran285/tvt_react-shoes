import React, { Component } from "react";
import Shoes from "./Shoes";

export default class ShoesList extends Component {
    renderShoesList = () => {
        return this.props.dataShoes.map((shoes, index) => {
          return (
           <Shoes shoes={shoes} key={index} themGioHang={this.props.themGioHang}/>
          );
        });
      };
  render() {
    return (
      <div>
        <div className="row">{this.renderShoesList()}</div>
      </div>
    );
  }
}
